package task3to110722.services;

import task3to110722.dto.StudentSignUp;

public interface StudentsService {
    void signUp(StudentSignUp form);
}


package task3to110722.services;

import task3to110722.dto.StudentSignUp;
import task3to110722.models.Student;
import task3to110722.repositories.StudentsRepository;
import task3to110722.repositories.StudentsRepositoryJdbcImpl;

public class StudentsServiceImpl implements StudentsService {

    private final StudentsRepository studentsRepository;

    public StudentsServiceImpl(StudentsRepository studentsRepository) {
        this.studentsRepository = studentsRepository;
    }

    @Override
    public void signUp(StudentSignUp form) {
        Student student = new Student(form.getFirstName(),
                form.getLastName(),
                form.getEmail(),
                form.getPassword());

        studentsRepository.save(student);
    }
}

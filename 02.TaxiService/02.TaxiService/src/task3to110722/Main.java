package task3to110722;

import task3to110722.jdbs.SimpleDataSource;
import task3to110722.models.Student;
import task3to110722.repositories.StudentsRepository;
import task3to110722.repositories.StudentsRepositoryJdbcImpl;
import task3to110722.services.StudentsService;
import task3to110722.services.StudentsServiceImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("resources\\db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(dataSource);
        StudentsService studentsService = new StudentsServiceImpl(studentsRepository);

        Student student = studentsRepository.findById(1L).orElseThrow(IllegalArgumentException::new);
        System.out.println(student);

        System.out.println(studentsRepository.findById(2L));
        Student stud1 = new Student(2L, "kamila", "dmitrieva", 19);
        studentsRepository.update(stud1);
        System.out.println(studentsRepository.findById(2L));

        studentsRepository.delete(4L);

        System.out.println(studentsRepository.findAllByAgeGreaterThanOrderByIdDesc(24));

    }
}

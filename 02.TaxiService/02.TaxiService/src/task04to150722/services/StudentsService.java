package task04to150722.services;

import task04to150722.dto.StudentSignUp;

public interface StudentsService {
    void signUp(StudentSignUp form);
}

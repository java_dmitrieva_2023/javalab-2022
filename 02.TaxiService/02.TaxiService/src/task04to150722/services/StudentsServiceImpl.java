package task04to150722.services;

import task04to150722.dto.StudentSignUp;
import task04to150722.models.Student;
import task04to150722.repositories.StudentsRepository;


public class StudentsServiceImpl implements StudentsService {

    private final StudentsRepository studentsRepository;

    public StudentsServiceImpl(StudentsRepository studentsRepository) {
        this.studentsRepository = studentsRepository;
    }

    @Override
    public void signUp(StudentSignUp form) {
        Student student = Student.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail())
                .password(form.getPassword())
                .build();

        studentsRepository.save(student);
    }
}

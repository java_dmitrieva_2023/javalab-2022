package task04to150722;

import task04to150722.jdbs.SimpleDataSource;
import task04to150722.models.Student;
import task04to150722.repositories.StudentsRepository;
import task04to150722.repositories.StudentsRepositoryJdbcTemplateImpl;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcTemplateImpl(dataSource);

        Student student = Student.builder()
                .firstName("Hi!")
                .lastName("Bye")
                .email("email")
                .password("qwe55684")
                .build();

        System.out.println(student);
        studentsRepository.save(student);
        System.out.println(student);

        System.out.println(studentsRepository.findById(4L).orElseThrow(IllegalArgumentException::new));
        Student studentForUpdate = Student.builder()
                .id(4L)
                .firstName("hossam")
                .lastName("eid")
                .age(21)
                .build();

        studentsRepository.update(studentForUpdate);
        System.out.println(studentsRepository.findById(4L).orElseThrow(IllegalArgumentException::new));

        studentsRepository.delete(5L);

        System.out.println(studentsRepository.findAllByAgeGreaterThanOrderByIdDesc(23));


    }
}
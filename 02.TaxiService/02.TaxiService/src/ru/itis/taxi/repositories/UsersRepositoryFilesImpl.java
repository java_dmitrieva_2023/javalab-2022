package ru.itis.taxi.repositories;

import com.sun.security.auth.UnixNumericUserPrincipal;
import ru.itis.taxi.models.User;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;
public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;


    private static final Function<User, String> userToString = user ->
            user.getId()
                    + "|" + user.getFirstName()
                    + "|" + user.getLastName()
                    + "|" + user.getEmail()
                    + "|" + String.valueOf(user.getPassword());

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try (FileReader fileReader = new FileReader(fileName)) {
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            String[] split;
            while ((line = bufferedReader.readLine()) != null) {
                split = line.split("\\|");
                users.add(new User(UUID.fromString(split[0]), split[1], split[2], split[3], split[4].toCharArray()));
            }
            return users;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public void save(User entity) {
        entity.setId(UUID.randomUUID());
        if (Files.exists(Path.of("users.txt"))) {
            findAll().stream().filter(user -> user.getEmail().equals(entity.getEmail())).findFirst().ifPresent(user -> {
                throw new IllegalArgumentException("User with this email already exists");
            });
        }
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = userToString.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {
        List<User> users = findAll();
        for (User user : users) {
            if (user.getId().equals(entity.getId())) {
                user.setFirstName(entity.getFirstName());
                user.setLastName(entity.getLastName());
                user.setEmail(entity.getEmail());
                user.setPassword(entity.getPassword());
            }
        }
        try (Writer writer = new FileWriter(fileName, false);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            for (User user : users) {
                String userAsString = userToString.apply(user);
                bufferedWriter.write(userAsString);
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(User entity) {
        List<User> users = findAll();
        for (User user : users) {
            if (user.getId().equals(entity.getId())) {
                users.remove(user);
            }
        }
        try (Writer writer = new FileWriter(fileName, false);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            for (User user : users) {
                String userAsString = userToString.apply(user);
                bufferedWriter.write(userAsString);
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void deleteById(UUID id) {
        List<User> users = findAll();
        List<User> copy = new ArrayList<>(users);
        for (User user : users) {
            if (user.getId().equals(id)) {
                copy.remove(user);
            }
        }
        try (Writer writer = new FileWriter(fileName, false);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            for (User user : copy) {
                String userAsString = userToString.apply(user);
                bufferedWriter.write(userAsString);
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public User findById(UUID id) {
        List<User> users = findAll();
        for (User user : users) {
            if (user.getId().equals(id)) {
                return user;
            }
        }
        return null;
    }
}



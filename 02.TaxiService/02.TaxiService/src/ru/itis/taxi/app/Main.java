package ru.itis.taxi.app;

import ru.itis.taxi.dto.SignUpForm;
import ru.itis.taxi.mappers.Mappers;
import ru.itis.taxi.models.User;
import ru.itis.taxi.repositories.UsersRepository;
import ru.itis.taxi.repositories.UsersRepositoryFilesImpl;
import ru.itis.taxi.services.UsersService;
import ru.itis.taxi.services.UsersServiceImpl;

import java.util.UUID;

public class Main {

    public static void main(String[] args) {
        System.out.println(UUID.randomUUID());
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);

        usersService.signUp(new SignUpForm("Камила", "Дмитриева",
                "dmitrieva.kam@mail.com", "kdt2003"));

        usersService.signUp(new SignUpForm("Камила", "Дмитриева",
                "dmitri@mail.ru", "k2003"));

        System.out.println(usersRepository.findAll());
        User getSecondUser = usersRepository.findAll().get(1);
        System.out.println(usersRepository.findById(getSecondUser.getId()));
        User user = new User(getSecondUser.getId(), "Камила", "Дмитриева", "dkt33@gmail.com", "qwewqe".toCharArray());
        usersRepository.update(user);
        System.out.println(usersRepository.findAll());
        usersRepository.deleteById(getSecondUser.getId());
        usersRepository.delete(new User("Камила", "Дмитриева", "dmitrieva.kam@gmail.com", "kdt2003".toCharArray()));
        System.out.println(usersRepository.findAll());
    }
}


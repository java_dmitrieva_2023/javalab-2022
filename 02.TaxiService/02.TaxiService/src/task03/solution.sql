select model, speed, hd
from pc
where price < 500

select distinct maker
from product
where type like 'printer'

select model, ram, screen
from laptop
where price > 1000

select *
from printer
where color like 'y'

select model, speed, hd
from pc
where cd like '12x' and price < 600
   or cd like '24x' and price < 600

select distinct maker, speed
from product p
         right join laptop l on l.model = p.model
where l.hd >= 10

SELECT distinct PC.model, price
FROM Product
         INNER JOIN
     PC ON PC.model = Product.model and Product.maker like 'B'
union
select distinct Laptop.model, price
from Product
         inner join
     laptop on Laptop.model = product.model and product.maker like 'B'
union
select distinct Printer.model, price
from Product
         inner join
     Printer on Printer.model = product.model and product.maker like 'B'

SELECT DISTINCT maker
FROM Product
WHERE type like 'PC'
  AND maker NOT IN (SELECT maker FROM Product WHERE type like 'Laptop')

SELECT distinct maker
FROM Product
         INNER JOIN
     PC ON PC.model = Product.model and PC.speed >= 450

select model, price
from printer
where price = (select max(price) from printer)

select (select avg(speed) from pc) as result

select (select avg(speed) from laptop where price > 1000) as result

SELECT avg(speed)
FROM Product
         INNER JOIN
     PC ON PC.model = Product.model and Product.maker like 'A'

SELECT Classes.class, name, country
FROM Classes
         INNER JOIN
     Ships ON Ships.class = Classes.class
where Classes.numGuns >= 10

select distinct pc1.hd
from PC pc1,
     PC pc2
where pc1.hd = pc2.hd
  and pc1.code != pc2.code

SELECT DISTINCT p1.model, p2.model, p1.speed, p1.ram
FROM pc p1,
     pc p2
WHERE p1.speed = p2.speed
  AND p1.ram = p2.ram
  AND p1.model > p2.model

SELECT DISTINCT p1.model, p2.model, p1.speed, p1.ram
FROM pc p1,
     pc p2
WHERE p1.speed = p2.speed
  AND p1.ram = p2.ram
  AND p1.model > p2.model

SELECT DISTINCT product.maker, printer.price
FROM product,
     printer
WHERE product.model = printer.model
  AND printer.color = 'y'
  AND printer.price = (SELECT MIN(price)
                       FROM printer
                       WHERE printer.color = 'y')

SELECT product.maker,
       AVG(screen)
FROM laptop
         LEFT JOIN product ON product.model = laptop.model
GROUP BY product.maker

SELECT maker, COUNT(model)
FROM product
WHERE type = 'pc'
GROUP BY product.maker
HAVING COUNT(DISTINCT model) >= 3

SELECT product.maker, MAX(pc.price)
FROM product,
     pc
WHERE product.model = pc.model
GROUP BY product.maker

SELECT pc.speed, AVG(pc.price)
FROM pc
WHERE pc.speed > 600
GROUP BY pc.speed

SELECT DISTINCT maker
FROM product t1
         JOIN pc t2 ON t1.model = t2.model
WHERE speed >= 750
  AND maker IN
      (SELECT maker
       FROM product t1
                JOIN laptop t2 ON t1.model = t2.model
       WHERE speed >= 750)

SELECT model
FROM (SELECT model, price
      FROM pc
      UNION
      SELECT model, price
      FROM Laptop
      UNION
      SELECT model, price
      FROM Printer) t1
WHERE price = (SELECT MAX(price)
               FROM (SELECT price
                     FROM pc
                     UNION
                     SELECT price
                     FROM Laptop
                     UNION
                     SELECT price
                     FROM Printer) t2)

SELECT DISTINCT maker
FROM product
WHERE model IN (SELECT model
                FROM pc
                WHERE ram = (SELECT MIN(ram)
                             FROM pc)
                  AND speed = (SELECT MAX(speed)
                               FROM pc
                               WHERE ram = (SELECT MIN(ram)
                                            FROM pc)))
  AND maker IN (SELECT maker
                FROM product
                WHERE type = 'printer')

SELECT sum(s.price) / sum(s.kol) as sredn
FROM (SELECT price, 1 as kol
      FROM pc,
           product
      WHERE pc.model = product.model
        AND product.maker = 'A'
      UNION all
      SELECT price, 1 as kol
      FROM laptop,
           product
      WHERE laptop.model = product.model
        AND product.maker = 'A') as s

SELECT product.maker, AVG(pc.hd)
FROM pc,
     product
WHERE product.model = pc.model
  AND product.maker IN (SELECT DISTINCT maker
                        FROM product
                        WHERE product.type = 'printer')
GROUP BY maker

Select count(maker) as f1
from (select maker from product group by maker having count(distinct model) <= 1) t1

SELECT t1.point, t1.date, inc, out
FROM income_o t1
         LEFT JOIN outcome_o t2 ON t1.point = t2.point
    AND t1.date = t2.date
UNION
SELECT t2.point, t2.date, inc, out
FROM income_o t1
         RIGHT JOIN outcome_o t2 ON t1.point = t2.point
    AND t1.date = t2.date

select point, date, SUM (sum_out), SUM (sum_inc)
from ( select point, date, SUM (inc) as sum_inc, null as sum_out from Income Group by point, date
    Union
    select point, date, null as sum_inc, SUM (out) as sum_out from Outcome Group by point, date ) as t
group by point, date
order by point

SELECT DISTINCT class, country
FROM classes
WHERE bore >= 16

Select country, cast(avg((power(bore, 3) / 2)) as numeric(6, 2)) as weight
from (select country, classes.class, bore, name
      from classes
               left join ships on classes.class = ships.class
      union all
      select distinct country, class, bore, ship
      from classes t1
               left join outcomes t2 on t1.class = t2.ship
      where ship = class
        and ship not in (select name from ships)) a
where name IS NOT NULL
group by country

SELECT o.ship
FROM BATTLES b
         LEFT join outcomes o ON o.battle = b.name
WHERE b.name = 'North Atlantic'
  AND o.result = 'sunk'

Select name
from classes,
     ships
where launched >= 1922
  and displacement > 35000
  and type = 'bb'
  and ships.class = classes.class

SELECT model, type
FROM product
WHERE upper(model) NOT like '%[^A-Z]%'
   OR model not like '%[^0-9]%'

Select name
from ships
where class = name
union
select ship as name
from classes,
     outcomes
where classes.class = outcomes.ship

SELECT c.class
FROM classes c
         LEFT JOIN (SELECT class, name
                    FROM ships
                    UNION
                    SELECT ship, ship
                    FROM outcomes) AS s ON s.class = c.class
GROUP BY c.class
HAVING COUNT(s.name) = 1

SELECT country
FROM classes
GROUP BY country
HAVING COUNT(DISTINCT type) = 2 WITH b_s AS
(SELECT o.ship, b.name, b.date, o.result
FROM outcomes o
LEFT JOIN battles b ON o.battle = b.name )
SELECT DISTINCT a.ship
FROM b_s a
WHERE UPPER(a.ship) IN
      (SELECT UPPER(ship)
       FROM b_s b
       WHERE b.date < a.date
         AND b.result = 'damaged')

SELECT maker, MAX(type)
FROM product
GROUP BY maker
HAVING COUNT(DISTINCT type) = 1
   AND COUNT(model) > 1

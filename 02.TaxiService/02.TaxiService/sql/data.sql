insert into client(id, email, firstname, lastname, password)
VALUES (gen_random_uuid(), 'kamila.dmitrieva@gmail.com', 'Kamila', 'Dmitrieva', 'qwerty');

insert into booking(id, date, destination, duration, location, price)
VALUES (gen_random_uuid(), current_timestamp, 'Kremlevskaya, 18', current_timestamp, 'Kremlevskaya, 120', 120);

insert into driver(id, firstname, lastname, phonenumber, rating)
VALUES (gen_random_uuid(), 'Ivan', 'Ivanov', '+79001233232', 5);

insert into vehicle(id, amountofseats, color, price, type)
VALUES (gen_random_uuid(), 4, 'Black', 570000, 'Business');

update vehicle
set amountofseats = 2
where amountofseats = 4;

update driver
set firstname = 'Max'
where firstname like 'Ivan';

update client
set lastname = 'Dmitrievaa'
where lastname like 'Dmitrieva';

update booking
set price = 200
where price = 120;
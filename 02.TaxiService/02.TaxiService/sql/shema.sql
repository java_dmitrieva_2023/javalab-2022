drop table if exists booking;
drop table if exists client;
drop table if exists driver;
drop table if exists vehicle;

create table client
(
    id        uuid         not null
        primary key,
    email     varchar(255) not null
        unique,
    firstname varchar(255) not null,
    lastname  varchar(255) not null,
    password  varchar(255) not null
);

alter table client
    owner to postgres;

create table driver
(
    id           uuid         not null
        primary key,
    firstname    varchar(255) not null,
    lastname     varchar(255) not null,
    phone_number varchar(255) not null,
    rating       smallint
);

alter table driver
    owner to postgres;

create table vehicle
(
    id              uuid         not null
        primary key,
    amount_of_seats integer      not null,
    color           varchar(255) not null,
    price           bigint       not null,
    type            varchar(255) not null
);

alter table vehicle
    owner to postgres;

create table booking
(
    id          uuid         not null
        primary key,
    date        timestamp    not null,
    destination varchar(255) not null,
    duration    timestamp    not null,
    location    varchar(255) not null,
    price       integer      not null
);

alter table booking
    owner to postgres;


